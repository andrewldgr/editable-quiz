const SERVER = "http://localhost:8080";//./quizAPI/";

function buttonLoading(btn){
    let childs = btn.childNodes;
    childs[1].style.display = "none";
    btn.classList.add('disabled');
    childs[0].classList.add('spinner-border');
    childs[0].classList.add('spinner-border-sm');
    childs[0].classList.add('text-primary');
}
function buttonLoaded(btn){
    btn.classList.remove('disabled');
    let childs = btn.childNodes;
    childs[1].style.display = "inline";
    childs[0].classList.remove('spinner-border');
    childs[0].classList.remove('spinner-border-sm');
    childs[0].classList.remove('text-primary');
}

function newQOverlay() {
    document.getElementById("overlay").style.display = "block";
    document.getElementById("overlayTitle").innerHTML = "Create Question";
    document.getElementById("saveNewQBtn").style.display = "block";
    document.getElementById("deleteQBtn").style.display = "none";
    document.getElementById("saveChangeBtn").style.display = "none";
    document.getElementById("cancelQBtn").style.display = "block";
    
}

function editQOverlay(qNum) {
    document.getElementById("overlay").style.display = "block";
    document.getElementById("overlayTitle").innerHTML = "Edit Question #"+qNum;
    document.getElementById("saveNewQBtn").style.display = "none";
    document.getElementById("deleteQBtn").style.display = "block";
    document.getElementById("saveChangeBtn").style.display = "block";
    document.getElementById("cancelQBtn").style.display = "block";
}

function overlayOff() {
    document.getElementById("overlay").style.display = "none";
}

function flushQEditDialog(){
    document.getElementById("qTextEditor").value = "";
    document.getElementById("qCodeEditor").value = "";
    document.getElementById("numAnswers").value = 4;
    document.getElementById("a1").style.display = "flex";
    document.getElementById("a2").style.display = "flex";
    document.getElementById("a3").style.display = "flex";
    document.getElementById("a4").style.display = "flex";
    document.getElementById("a1Text").value = "";
    document.getElementById("a2Text").value = "";
    document.getElementById("a3Text").value = "";
    document.getElementById("a4Text").value = "";

    setCorrectButtonPositive(document.getElementById("a1Correct"));
    setCorrectButtonNegative(document.getElementById("a2Correct"));
    setCorrectButtonNegative(document.getElementById("a3Correct"));
    setCorrectButtonNegative(document.getElementById("a4Correct"));
}

function toggleCorrectBtn(btn){
    btn.classList.toggle('btn-bad');
    btn.classList.toggle('btn-primary');
    if (btn.value == "true") {
        btn.value = "false";
    } else {
        btn.value = "true"
    }

    let childs = btn.childNodes;
    childs[0].classList.toggle('fa-times');
    childs[0].classList.toggle('fa-check');
}
function setCorrectButtonPositive(btn){
    btn.classList.remove('btn-bad');
    btn.classList.add('btn-primary');
    btn.value = "true";
    let childs = btn.childNodes;
    childs[0].classList.remove('fa-times');
    childs[0].classList.add('fa-check');
}
function setCorrectButtonNegative(btn){
    btn.classList.remove('btn-primary');
    btn.classList.add('btn-bad');
    btn.value = "false";
    let childs = btn.childNodes;
    childs[0].classList.remove('fa-check');
    childs[0].classList.add('fa-times');
}


function setAnswerNumber(num) {
    document.getElementById("numAnswers").value = num;
    if (num == 2) {
        document.getElementById("a1").style.display = "flex";
        document.getElementById("a2").style.display = "flex";
        document.getElementById("a3").style.display = "none";
        document.getElementById("a4").style.display = "none";
    }
    if (num == 3) {
        document.getElementById("a1").style.display = "flex";
        document.getElementById("a2").style.display = "flex";
        document.getElementById("a3").style.display = "flex";
        document.getElementById("a4").style.display = "none";
    }
    if (num == 4) {
        document.getElementById("a1").style.display = "flex";
        document.getElementById("a2").style.display = "flex";
        document.getElementById("a3").style.display = "flex";
        document.getElementById("a4").style.display = "flex";
    }
}

function deleteQuestion() {
    
    let qID = document.getElementById("qIDHolder").value;
    return new Promise( function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        let delData = {"function": "DELETEQ", "qid": qID};
        xmlHttp.open("DELETE", SERVER, true);
        xmlHttp.send(JSON.stringify(delData));
    });
}

function updateQuestion() {

    //Info gathering
    let qID = document.getElementById("qIDHolder").value;
    let qText = document.getElementById("qTextEditor").value;
    let codeText = document.getElementById("qCodeEditor").value;
    let numA = document.getElementById("numAnswers").value;
    let textArr = [
        document.getElementById("a1Text").value,
        document.getElementById("a2Text").value,
        document.getElementById("a3Text").value,
        document.getElementById("a4Text").value
    ];
    let correctArr = [
        document.getElementById("a1Correct").value,
        document.getElementById("a2Correct").value,
        document.getElementById("a3Correct").value,
        document.getElementById("a4Correct").value
    ];
    let aKey = [
        document.getElementById("a1").value,
        document.getElementById("a2").value,
        document.getElementById("a3").value,
        document.getElementById("a4").value
    ];

    //replace question
    qPromise = new Promise( function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        let putData = {"function": "UPDATEQ", "qid": qID, "qtext": qText, "codetext": codeText};
        xmlHttp.open("PUT", SERVER, true);
        xmlHttp.send(JSON.stringify(putData));
    });

    //replace answers
    let repAnsObj = {'function': 'REPLACEANSWERS', 'qid': qID, 'aid': new Array(), 'atext': new Array(), 'iscorrect': new Array()};
    for (let i = 0; i < numA; i++) {
        
        repAnsObj['aid'].push(aKey[i]);
        repAnsObj['atext'].push(textArr[i]);
        repAnsObj['iscorrect'].push(correctArr[i]);
        
    }
    aPromise = replaceAnswers(repAnsObj);

    //return status of all promises
    return Promise.all([aPromise, qPromise]);
}

function replaceAnswers(ansObj){
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("PUT", SERVER, true);

        //xmlHttp.setRequestHeader("content-type", "application/json;charset=UTF-8"); 
        //"function=UPDATEANS&aid="+aID+"&qid="+qID+"&atext="+aText+"&iscorrect="+isCorrect
        xmlHttp.send(JSON.stringify(ansObj));
    });
}

function updateAnswer(aID, qID, aText, isCorrect){
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        let putData = {"function": "UPDATEANS", "aid": aID, "qid": qID, "atext": aText, "iscorrect": isCorrect};
        xmlHttp.open("PUT", SERVER, true);

        //xmlHttp.setRequestHeader("content-type", "application/json;charset=UTF-8"); 
        //"function=UPDATEANS&aid="+aID+"&qid="+qID+"&atext="+aText+"&iscorrect="+isCorrect
        xmlHttp.send(JSON.stringify(putData));
    });
}

function saveNewQuestion() {

    let qText = document.getElementById("qTextEditor").value;
    let codeText = document.getElementById("qCodeEditor").value;

    return new Promise( function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("POST", SERVER, true);
        //xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8"); 
        xmlHttp.send("function=NEWQ&qtext="+qText+"&codetext="+codeText);
    });
}

function saveNewAnswers(qID) {
    let numA = document.getElementById("numAnswers").value;
    let textArr = [
        document.getElementById("a1Text").value,
        document.getElementById("a2Text").value,
        document.getElementById("a3Text").value,
        document.getElementById("a4Text").value
    ];
    let correctArr = [
        document.getElementById("a1Correct").value,
        document.getElementById("a2Correct").value,
        document.getElementById("a3Correct").value,
        document.getElementById("a4Correct").value
    ];
    let promiseArr = [];
    for (let i = 0; i < numA; i++) {
        promiseArr[i] = saveNewAnswer(qID, textArr[i], correctArr[i])
        .then(function(resolveText) {
            resolve(xmlHttp.responseText);
        }, function(errorText) {
            reject(xmlHttp.status);
        });
    }

    return Promise.all(promiseArr)
    .then(function(resolveText) {
        resolve(xmlHttp.responseText);
        reject(xmlHttp.status);
    }, function(errorText) {
        
    });
}

function saveNewAnswer(qID, aText, isCorrect){
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("POST", SERVER, true);
        xmlHttp.send("function=NEWA&qid="+qID+"&atext="+aText+"&iscorrect="+isCorrect);
    });
}

function editQuestion(qID, dispID, qText, codeText) {
    //Curry function as it's invoked inside a loop's click listener
    return function () {
        editQOverlay(dispID);

        document.getElementById("qIDHolder").value = qID;
        document.getElementById("qTextEditor").value = qText;
        document.getElementById("qCodeEditor").value = codeText;

        loadAnswers(qID)
        .then( function(resolveText) {
            let aObject = JSON.parse(resolveText);
            setAnswerNumber(Object.keys(aObject).length);
            for (i=0; i<Object.keys(aObject).length; i++){
                document.getElementById("a"+(i+1)).value = aObject[i].aID;
                document.getElementById("a"+(i+1)+"Text").value = aObject[i].aText;
                if (aObject[i].isCorrect == "true") {
                    setCorrectButtonPositive(document.getElementById("a"+(i+1)+"Correct"));
                } else {
                    setCorrectButtonNegative(document.getElementById("a"+(i+1)+"Correct"));
                }
            }
        }, function (errorText) {
            alert ("unable to load answer: "+errorText);
        });

    }
}

function loadAnswers(qID) {
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("GET", SERVER+"?function=GETANSWERS&qid="+qID, true);
        xmlHttp.send("");
    });
}
function displayAnswers(aObject){

    for (i=0; i<Object.keys(qObject).length; i++){
        if (qObject[i].codeText != "") {
            qObject[i].codeText = '<div class="code">'+qObject[i].codeText+'</div>';
        }
        displayStr = 
        '<div class="card bg-light">'
        +'<div class="card-body">'
            +'<h4 class="card-title">Question #'+(i+1)+'</h4>'
            +'<p class="card-text">'+qObject[i].qText+'</p>'
            +qObject[i].codeText
            +'<a id="q'+i+'EditBtn" class="btn btn-warning float-right">Edit Question</a>'
        +'</div>'
        +'</div>'

        document.getElementById('questionsContent').insertAdjacentHTML('beforeend', displayStr);
        //Use Curry Function to bind values
        document.getElementById('q'+i+'EditBtn').addEventListener("click", editQuestion(qObject[i].qID, (i+1), qObject[i].qText, qObject[i].codeText)
        );
    }
}

function loadQuestions(){
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("GET", SERVER+"?function=GETINFO", true);
        xmlHttp.send("");
    });
}

function displayQuestions(qObject) {
    let displayStr = "";
    let codeStr = "";

    for (i=0; i<Object.keys(qObject).length; i++){
        if (qObject[i].codeText != "") {
            codeStr = '<div class="code">'+qObject[i].codeText+'</div>';
        } else {
            codeStr = "";
        }

        displayStr = 
        '<div class="card bg-light">'
        +'<div class="card-body">'
            +'<h4 class="card-title">Question #'+(i+1)+'</h4>'
            +'<p class="card-text">'+qObject[i].qText+'</p>'
            +codeStr
            +'<a id="q'+i+'EditBtn" class="btn btn-warning float-right">Edit Question</a>'
        +'</div>'
        +'</div><br />';

        document.getElementById('questionsContent').insertAdjacentHTML('beforeend', displayStr);
        //Use Curry Function to bind values
        document.getElementById('q'+i+'EditBtn').addEventListener("click", editQuestion(qObject[i].qID, (i+1), qObject[i].qText, qObject[i].codeText)
        );
    }
}

function refreshQuestions() {

    document.getElementById("questionsContent").innerHTML = "";
    document.getElementById("questionsContent").classList.add("spinner-border");
    document.getElementById("questionsContent").classList.add("text-primary");
    loadQuestions()
    .then( function(resolveText) {
        let qObject = JSON.parse(resolveText);
        displayQuestions(qObject);
        
        document.getElementById("questionsContent").classList.remove("spinner-border");
        document.getElementById("questionsContent").classList.remove("text-primary");
    }, function(errorText) {
        alert("Could not retrieve questions on update");
        document.getElementById("questionsContent").classList.remove("spinner-border");
        document.getElementById("questionsContent").classList.remove("text-primary");
    });
}

window.onload = function(event) {
    
    document.getElementById("newQBtn").addEventListener("click", function() {
        newQOverlay();
    });
    document.getElementById("cancelQBtn").addEventListener("click", function() {
        flushQEditDialog();
        overlayOff();
    });
    document.getElementById("saveChangeBtn").addEventListener("click", function() {
        //Disable button and replace its icon with a loading image
        buttonLoading(this); 
    
        updateQuestion()
        .then( function(resolveText) {
            flushQEditDialog();
            buttonLoaded(document.getElementById("saveChangeBtn"));
            overlayOff();
            refreshQuestions();
            
        }, function(errorText){
            alert("Error: Could not store question or answers: "+errorText);
            flushQEditDialog();
            buttonLoaded(document.getElementById("saveChangeBtn"));
            overlayOff();
        });
    })
    document.getElementById("saveNewQBtn").addEventListener("click", function() {
    
        //Disable button and replace its icon with a loading image
        buttonLoading(this); 
    
        saveNewQuestion()
        .then( function(resolveText) {
            
            saveNewAnswers(resolveText)
            .then( function(resolveText) {
                flushQEditDialog();
                buttonLoaded(document.getElementById("saveNewQBtn"));
                overlayOff();
                refreshQuestions();
            }, function(errorText){
                alert("Error: Could not store answers: "+errorText);
                flushQEditDialog();
                buttonLoaded(document.getElementById("saveNewQBtn"));
                overlayOff();
            });
            
        }, function(errorText){
            alert("Error: Could not store question: "+errorText);
            flushQEditDialog();
            buttonLoaded(document.getElementById("saveNewQBtn"));
            overlayOff();
        });
    });
    
    document.getElementById("deleteQBtn").addEventListener("click", function() {
        buttonLoading(this); 

        let r=confirm("Are you sure you want to delete this question?");
        if (r==true)
        {
            deleteQuestion()
            .then( function(resolveText) {
                flushQEditDialog();
                buttonLoaded(document.getElementById("deleteQBtn"));
                overlayOff();
                refreshQuestions();
                
            }, function(errorText){
                alert("Error: Could not store question or answers: "+errorText);
                flushQEditDialog();
                buttonLoaded(document.getElementById("deleteQBtn"));
                overlayOff();
            });
        }
        else
        {
            flushQEditDialog();
            buttonLoaded(document.getElementById("deleteQBtn"));
            overlayOff();
        }
    
        
    })
    
    document.getElementById("numAnswers").addEventListener("click", function() {
        setAnswerNumber(document.getElementById("numAnswers").value);
    });
    document.getElementById("a1Correct").value = "true";
    document.getElementById("a1Correct").addEventListener("click", function() {
        setCorrectButtonPositive(this);
        setCorrectButtonNegative(document.getElementById("a2Correct"));
        setCorrectButtonNegative(document.getElementById("a3Correct"));
        setCorrectButtonNegative(document.getElementById("a4Correct"));
    });
    document.getElementById("a2Correct").addEventListener("click", function() {
        setCorrectButtonPositive(this);
        setCorrectButtonNegative(document.getElementById("a1Correct"));
        setCorrectButtonNegative(document.getElementById("a3Correct"));
        setCorrectButtonNegative(document.getElementById("a4Correct"));
    });
    document.getElementById("a3Correct").addEventListener("click", function() {
        setCorrectButtonPositive(this);
        setCorrectButtonNegative(document.getElementById("a1Correct"));
        setCorrectButtonNegative(document.getElementById("a2Correct"));
        setCorrectButtonNegative(document.getElementById("a4Correct"));
    });
    document.getElementById("a4Correct").addEventListener("click", function() {
        setCorrectButtonPositive(this);
        setCorrectButtonNegative(document.getElementById("a1Correct"));
        setCorrectButtonNegative(document.getElementById("a2Correct"));
        setCorrectButtonNegative(document.getElementById("a3Correct"));
    });

    loadQuestions()
    .then( function(resolveText) {
        let qObject = JSON.parse(resolveText);
        displayQuestions(qObject);
        
        document.getElementById("questionsContent").classList.remove("spinner-border");
        document.getElementById("questionsContent").classList.remove("text-primary");
    }, function(errorText) {
        alert("Could not retrieve questions");
        document.getElementById("questionsContent").classList.remove("spinner-border");
        document.getElementById("questionsContent").classList.remove("text-primary");
    });
}
