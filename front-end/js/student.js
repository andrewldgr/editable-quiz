const SERVER = "http://localhost:8080";//./quizAPI/";

function jsHighlight(text) {
    //let, var, const, for, {, }, (, ), +, -, * , / ,= 
    text = text.replace("/", "<code>/</code>");         //this one has to be first
    text = text.replace("=", "<code>=</code>");
    text = text.replace(/let/gi, "<code>let</code>");
    text = text.replace(/var/gi, "<code>var</code>");
    text = text.replace(/const/gi, "<code>const</code>");
    text = text.replace(/for/gi, "<code>for</code>");
    text = text.replace(/{/gi, "<code>{</code>");
    text = text.replace(/}/gi, "<code>}</code>");
    text = text.replace("(", "<code>(</code>");
    text = text.replace(")", "<code>)</code>");
    text = text.replace("+", "<code>+</code>");
    text = text.replace("-", "<code>-</code>");
    text = text.replace("*", "<code>*</code>");
    text = text.replace("\\", "<code>\\</code>");
    
    
    

    return text;
}

function submitAnswers() {
    let numQuestions = document.getElementById("numQuestions").value;
    if (numQuestions == undefined) {
        numQuestions = 0;
    }
    let score = 0;
    let isCorrect = "";
    for (i=0; i < numQuestions; i++) {

        checkedElement = document.querySelector('input[name="answerSet'+i+'"]:checked');
        if(checkedElement != null && checkedElement.value == "true"){
            score ++;
        }
    }
    score = (score/numQuestions)*100;
    displayResults(score);
}

function displayResults(score) {
    document.getElementById("overlay").style.display = "block";
    document.getElementById("overlayTitle").innerHTML = "Your Score: "+score+"%";
    document.getElementById("scoreHolder").value = score;

    getScores()
    .then(function(resolveText) {
        let scoreObj = JSON.parse(resolveText);
        let scoreStr = "";
        for (i=0; i<Object.keys(scoreObj).length; i++){
            scoreStr += '<p>'+scoreObj[i]['name']+': '+scoreObj[i]['score']+'%</p>'
        }
        

        document.getElementById("scoreBox").innerHTML = scoreStr;
    }, function(errorText) {
        throw errorText;
    });

}

function getScores() {
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("GET", SERVER+"?function=GETSCORES", true);
        xmlHttp.send("");
    });
}

function submitScore() {
    buttonLoading(document.getElementById("saveScoreBtn"));
    let score = document.getElementById("scoreHolder").value;
    let name = document.getElementById("name").value;

    postScore(name, score)
    .then(function(resolveText){
        buttonLoaded(document.getElementById("saveScoreBtn"));
        overlayOff();
    }, function(errorText){
        throw errorText;
    });

}

function buttonLoading(btn){
    let childs = btn.childNodes;
    childs[1].style.display = "none";
    btn.classList.add('disabled');
    childs[0].classList.add('spinner-border');
    childs[0].classList.add('spinner-border-sm');
    childs[0].classList.add('text-primary');
}
function buttonLoaded(btn){
    btn.classList.remove('disabled');
    let childs = btn.childNodes;
    childs[1].style.display = "inline";
    childs[0].classList.remove('spinner-border');
    childs[0].classList.remove('spinner-border-sm');
    childs[0].classList.remove('text-primary');
}

function postScore(name, score){
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("POST", SERVER, true);
        xmlHttp.send("function=NEWSCORE&name="+name+"&score="+score);
    });
}

function displayQuestions(qObject) {
    let displayStr = "";
    let highlightedCode = "";
    let codeStr = "";


    document.getElementById("numQuestions").value = (Object.keys(qObject).length);

    for (i=0; i<Object.keys(qObject).length; i++){

        let answersStr = "";
        for (j=0; j < Object.keys(qObject[i].answers).length; j++){
            answersStr += 
            '<input type="radio" name="answerSet'+i+'" value="'+qObject[i].answers[j]['isCorrect']+'" /><label>'+qObject[i].answers[j]['aText']+'</label><br />';
        }

        if (qObject[i].codeText != "") {
            highlightedCode = jsHighlight(qObject[i].codeText);
            codeStr = '<div class="code">'+highlightedCode+'</div>';
        } else {
            codeStr = "";
        }

        displayStr = 
        '<div class="card bg-light">'
        +'<div class="card-body">'
            +'<p class="card-title">Question #'+(i+1)+'</p>'
            +'<h5 class="card-text">'+qObject[i].qText+'</h5>'
            +codeStr
            +'<div id=numAnswers'+(Object.keys(qObject[i].answers).length)+'>'+answersStr+'</div>'
        +'</div>'
        +'</div><br />';

        document.getElementById('questionsContent').insertAdjacentHTML('beforeend', displayStr);
    }
}

function loadAnswers(qID) {
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("GET", SERVER+"?function=GETANSWERS&qid="+qID, true);
        xmlHttp.send("");
    });
}

function loadQuestions(){
    return new Promise (function(resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                resolve(xmlHttp.responseText);
            } else if (xmlHttp.status >= 300) {
                reject(xmlHttp.status);
            }
        }
        xmlHttp.open("GET", SERVER+"?function=GETALLINFO", true);
        xmlHttp.send("");
    });
}

function overlayOff() {
    document.getElementById("overlay").style.display = "none";
}

window.onload = function(event) {
    
    document.getElementById("checkBtn").addEventListener("click", function() {
        submitAnswers();
    });
    document.getElementById("cancelBtn").addEventListener("click", function() {
        overlayOff();
    });
    document.getElementById("saveScoreBtn").addEventListener("click", function() {
        submitScore();
    })

    loadQuestions()
    .then( function(resolveText) {
        let qObject = JSON.parse(resolveText);
        displayQuestions(qObject);
        
        document.getElementById("questionsLoader").classList.remove("spinner-border");
        document.getElementById("questionsLoader").classList.remove("text-primary");
    }, function(errorText) {
        alert(errorText);
        document.getElementById("questionsLoader").classList.remove("spinner-border");
        document.getElementById("questionsLoader").classList.remove("text-primary");
    });
}
